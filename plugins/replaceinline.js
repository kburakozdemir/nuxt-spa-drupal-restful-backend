import Vue from 'vue'

function replaceAll(string, search, replace) {
  return string.split(search).join(replace)
}

Vue.filter('replaceinline', value => {
  const content = replaceAll(
    value,
    '/sites/default/files',
    'https://drupal.ist/sites/default/files'
  )

  const content2 = replaceAll(content, '<img', '<img class="img-fluid"')

  const content3 = replaceAll(
    content2,
    '<table',
    '<table class="table b-table"'
  )

  return content3
})
