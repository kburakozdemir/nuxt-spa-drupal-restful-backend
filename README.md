# nuxt-test

> My first Nuxt.js project

[![Netlify Status](https://api.netlify.com/api/v1/badges/53b71fd9-a552-4ad9-9b65-1b95b172e0db/deploy-status)](https://app.netlify.com/sites/kburakozdemir/deploys)

## Purpose

The aim of this project is building 100% static web site(s), via fetching dynamic content from any API, mainly from Drupal 8 REST or JSON API.

## Roadmap

1. Render all routes as %100 pure static html files in this project.
2. Integrate Drupal 8 headless API to serve data as the back-end.

## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

## Data

Data is being fetched from [JSONPlaceholder](https://jsonplaceholder.typicode.com/).

## Final Result

Automaticly build, deployed and staticly served by [netlify](https://www.netlify.com/).

URL is [https://kburakozdemir.netlify.com/](https://kburakozdemir.netlify.com/)

## Resources

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).

[Bootstrap + Vue](https://bootstrap-vue.js.org/)

[Serverless Forms](https://www.netlify.com/blog/2018/09/07/how-to-integrate-netlify-forms-in-a-vue-app/)

Apart from the official nuxt.js documentation, these may be helpfull for learning nuxt.js:

### Articles

1. [Porting my personal website to nuxt.js](https://dev.to/errietta/porting-my-personal-website-to-nuxtjs-29p5)
2. [How to generate a static website with Vue.js in no time](https://medium.freecodecamp.org/how-to-generate-a-static-website-with-vue-js-in-no-time-e74e7073b7b8)
3. [Build a Beautiful Website with VuePress and Tailwind.css](https://dev.to/vuevixens/build-a-beautiful-website-with-vuepress-and-tailwindcss--3a03)
4. [Summary of VueJS by Sarah Drasner - 7 Articles](https://medium.com/@j_lim_j/summary-of-vuejs-by-sarah-drasner-part-1-of-7-directives-data-rendering-30103729ec9b)
5. [Website with blog and portfolio using Vue.js + Nuxt + Markdown](https://marinaaisa.com/blog/blog-using-vue-nuxt-markdown)
6. [Going Full Static](https://nuxtjs.org/blog/going-full-static)
7. [Decoupled WordPress with Nuxt.js and Dat](https://scott.ee/journal/headless-wordpress-api-nuxt-dat/)
8. [Host your personal site for free with Nuxt.js, GitLab, and Cloudflare.](https://medium.com/@tskaggs/host-your-personal-site-for-free-with-nuxt-js-gitlab-and-cloudflare-7fe038cfd079)
9. [Build a DEV.TO clone with Nuxt new fetch](https://nuxtjs.org/blog/build-dev-to-clone-with-nuxt-new-fetch/)
10. [How To Create A Headless WordPress Site On The JAMstack](https://www.smashingmagazine.com/2020/02/headless-wordpress-site-jamstack/) **Sarah Drasner**
11. [Create a Blog with Contentful and Nuxt](https://www.netlify.com/blog/2020/04/20/create-a-blog-with-contentful-and-nuxt/) **Sarah Drasner**

### SEO Issues

A question in mind: Is SSR needed regarding Universal, PWA, SPA web apps?

[4 important articles about Google Bots' Content Crawling](https://mavo.io/faq/#seo)

### Interesting vue.js Examples

1. [parsing a csv =file import](https://codepen.io/edward1995/pen/QmXdwz)

### People to follow

1. [Alexander Lichter](https://blog.lichter.io/)

### Some Vue Based Theming Options

1. [Buefy - Lightweight UI components for Vue.js based on Bulma](https://buefy.org/)

### Drupal

1. [A Tutorial for Drupal 8 and Vue](https://watch-learn.com/series/one-page-app-with-drupal-8-and-vuejs)
2. [Contenta CMS](http://www.contentacms.org/)
3. [Contenta CMS Vue-Nuxt Repo](https://github.com/contentacms/contenta_vue_nuxt)
4. [Contenta CMS Vue-Nuxt Demo](https://contentanuxt.now.sh/)

### Courses

1. [Scrimba - Online Courses](https://scrimba.com/)
2. [Vue 2 and October CMS Todo App](https://watch-learn.com/series/vue-2-and-october-cms-todo-app)

## Built with Nuxt

1. [https://marinaaisa.com/](https://marinaaisa.com/)

## Thanks

### GitHub Issues for %100 Static Files

1. [SpikeShape's answer to a question](https://github.com/nuxt/nuxt.js/issues/2822#issuecomment-402210983)

Thanks to [SpikeShape](https://github.com/SpikeShape) for his kind support, regarding this [issue comment](https://github.com/nuxt/nuxt.js/issues/2822#issuecomment-402210983).

As he mentioned in an e-mail, this may be the solution for a 100% static pre-rendering:

>You can have a look at my webpack config file here:
>https://github.com/SpikeShape/vue-prerendering/blob/master/webpack.config.js
>
>Line 178 starts using the Prerender plugin I mentioned.
>The prerender plugin itself can be found over here:
>https://github.com/chrisvfritz/prerender-spa-plugin
